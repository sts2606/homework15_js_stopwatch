let timeNow = 0;
let flag = 0;
let milliseconds = document.getElementById("milliseconds");
let seconds = document.getElementById("seconds");
let minutes = document.getElementById("minutes");
let button = document.getElementById("startPauseBtn");
button.addEventListener("click", startTimer);
let clear = document.getElementById("clear");
clear.addEventListener("click", clearField);
let timerId;

function startTimer() {
    flag = 1;
    button.removeEventListener("click", startTimer);
    button.addEventListener("click", pauseTimer);
    button.classList.remove("start-btn");
    button.classList.add("pause-btn");
    let startTime = new Date();
    timerId = setInterval(function () {
        let currentTime = new Date();
        let timer = +currentTime - +startTime + +timeNow;
        if (flag) {
            let millisec = timer.toString();
            millisec.substr(-3) < 10 ? milliseconds.innerHTML = `00${millisec}.substr(-3)` : (millisec.substr(-3) < 100 ? milliseconds.innerHTML = `00${millisec}.substr(-3)` : milliseconds.innerHTML = millisec.substr(-3));
            milliseconds.innerHTML = millisec.substr(-3);
            let sec = Math.abs(Math.floor(timer / 1000) % 60);
            sec < 10 ? seconds.innerHTML = `0${sec}` : seconds.innerHTML = sec;
            let min = Math.abs(Math.floor(timer / 1000 / 60) % 60);
            min < 10 ? minutes.innerHTML = `0${min}` : minutes.innerHTML = min;
        } else {
            timeNow = timer;
            clearInterval(timerId);
            return;
        }
    }, 10)
}

function pauseTimer() {
    button.removeEventListener("click", pauseTimer);
    button.addEventListener("click", startTimer);
    button.classList.remove("pause-btn");
    button.classList.add("start-btn");
    flag = 0;
}

function clearField() {
    pauseTimer();
    clearInterval(timerId);
    timeNow = 0;
    milliseconds.innerHTML = "000";
    seconds.innerHTML = "00";
    minutes.innerHTML = "00";
}
